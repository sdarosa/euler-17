/*
 * 05-20-2014
 * Author: Stephania DaRosa
 * Euler Project #17: Number Letter Count
 * 					  If all the numbers from 1-1000 inclusive were written out in words, 
 * 					  how many letters would be used?
 */

package numberlettercount;

import java.util.ArrayList;
import java.util.Arrays;

public class NumberLetterCount {
	
	String[] nums1_20 = {"one","two","three","four","five","six","seven","eight","nine","ten","eleven","twelve","thirteen","fourteen","fifteen","sixteen","seventeen","eighteen","nineteen","twenty"};
	String[] nums20_90 = {"twenty", "thirty","forty","fifty","sixty","seventy","eighty","ninety"};
	String hundred = "hundred",
		   and = "and",
		   thousand = "thousand";
	
	
	//fills the arrayList with its respective number
	public void generateNums() {
		ArrayList<String> totalNumberValues = new ArrayList<>();
		
		//numbers 1 - 20
		for(int i=0; i < nums1_20.length; i++) {
			totalNumberValues.add(nums1_20[i]);
		}
		//numbers 21 - 99
		totalNumberValues.addAll(get21To99());
		
		//Numbers 100 - 999
		totalNumberValues.addAll(get100To999(totalNumberValues));
		
		totalNumberValues.add("onethousand");
		
		//print the arrayList
		//printArrayList(totalNumberValues, 1);	
		
		System.out.println("Total size is " + countLetters(totalNumberValues));
	}
	
	//printArrayList
	public void printArrayList(ArrayList<String> nums, int min) {
		int i,j;
		for(i=0, j=min; i<nums.size(); i++, j++) {
			System.out.println(j + ": " + nums.get(i));
		}	
	}
	
	//countLetters
	public int countLetters(ArrayList<String> numValues) {
		int num = 0;		
		for(int i=0; i< numValues.size(); i++) {
			System.out.println(i + ": " + numValues.get(i) + " -> size: " + numValues.get(i).length());
			num += numValues.get(i).length();
		}		
		return num;
	}
	
	//get 21 - 99
	public ArrayList<String> get21To99() {
		int i,j;
		String temp;
		ArrayList<String> numberValues = new ArrayList<>();
		
		for(i=0; i<nums20_90.length; i++) {
			for(j=0; j<9; j++) {
				temp = nums20_90[i] + nums1_20[j];
				numberValues.add(temp);				
			}
			if(i < (nums20_90.length - 1)) {
				temp = nums20_90[i + 1];
				numberValues.add(temp);				
			}
		}
		//printArrayList(numberValues, 21);
		return numberValues;
	}
	
	//get 100 - 999
	public ArrayList<String> get100To999(ArrayList<String> numbers1To99) {
		ArrayList<String> numberValues = new ArrayList<>();
		int i,j,k,l;	//counters			
		String temp = "";		
		//100, 200, ...
		for(i=0, k=0; i < 9; i++) {			
			for(j=0, l=0; j < 100; j++, k++) {
				if(k % 100 == 0) {
					temp = nums1_20[i] + hundred;					
				} else {
					temp = nums1_20[i] + hundred + and + numbers1To99.get(l);	
					l++;
				}
				numberValues.add(temp);
			}							
		}		
		return numberValues;
	}
	
	
	public void print3Arrays(String[] arr1, String[] arr2, int min) {
		int i,j,k;
		String temp;
		for(i=0, k=min; i<9; i++) {
			for(j=0; j<arr2.length; j++, k++) {
				temp = k + ": " + arr1[i] + " " + hundred + " ";
				if(k % 100 != 0) {
					temp += and + " ";
					
				}
				System.out.println(temp);
			}
		}
	}
	
	//count letters in a string
	public int countLetters(String word) {		
		return word.length();
	}
	
	//count letters in an array of strings
	public int countLettersArr(String[] numbers) {
		int numLetters = 0;
		for(int i=0; i<numbers.length; i++) {
			numLetters += numbers[i].length();
		}
		return numLetters;
	}
	
	

}
